<?php

namespace SausprShop\Categories\Model;

use \SausprShop\Framework\Helper\SqlBuilder;
use \SausprShop\Framework\Helper\Request;

class Category
{
    private $id;
    private $name;
    private $description;
    private $parentId;

    public function getProduct($id = '')
    {
        if ($id !== '') {
            $this->load($id);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getParentId()
    {
        return $this->parentId;
    }

    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 


    public function load($id)
    {
        if ($id === "All" || (array_key_last($_POST) === "showAll") && $id === "") {
            $this->loadAll();
        } else {
            $this->loadOne($id);
        }
    }

    public function loadOne($id)
    {
        if ($id !== null && $id !== "") {
            $db = new SqlBuilder();
            $category = $db->select()->from('categories')->where('id', $id)->getOne();
            $this->id = $category['id'];
            $this->name = $category['name'];
            $this->description = $category['description'];
            $this->parentId = $category['parent_id'];
            //   var_dump($product);
            if (array_key_last($_POST) !== "edit" && array_key_last($_POST) !== "update") {
                $table = "<table><tr>";
                foreach ($category as $header => $property) {
                    $table .= "<th>$header</th>";
                }
                $table .= "</tr><tr>";
                foreach ($category as $property) {
                    $table .= "<td>$property</td>";
                }
                $table .= "</tr></table>";
                echo $table;
            }
        }
    }

    public function loadAll()
    {
        $db = new SqlBuilder();
        $categories = $db->select()->from('categories')->getAll();
        $table = "<table class='table-stripped'>";
        $table .= "<tr>";
        foreach ($categories[0] as $header => $value) {
            $table .= "<th>$header</th>";
        }
        $table .= "<th>Edit</th><th>Delete</th></tr>";

        foreach ($categories as $category) {
            $table .= "<tr>";
            foreach ($category as $propertyValue) {
                $id = array_key_first($category);
                $table .= "<td>$propertyValue</td>";
            }

            $table .= "<td><form action='http://localhost:8001/index.php/categories/edit/$category[$id]' method='POST'><button type='submit' name='$category[$id]'>Edit</button></form></td>";
            $table .= "<td><form action='http://localhost:8001/index.php/categories/delete/$category[$id]' method='POST'><button type='submit' name='$category[$id]'>Delete</button></form></td></tr>";
        }
        $table .= "</table>";
        echo $table;
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->create();
        }
    }

    private function update()
    {
        $request = new Request();
        $id = $this->id;
        $category = [
            'name' => $request->getPost('name'),
            'description' => $request->getPost('description'),
            'parent_id' => $request->getPost('parent_id'),
        ];

        $db = new SqlBuilder();
        $db->update('categories')->set($category)->where('id', $id)->exec();
    }

    private function create()
    {
        $category = [
            'name' => $this->name,
            'description' => $this->description,
            'parent_id' => $this->parentId
        ];

        $db = new SqlBuilder();
        $db->insert('categories')->values($category)->exec();
    }

    public function delete($id)
    {
        $db = new SqlBuilder();
        $allIds = $db->select('id')->from('categories')->getAll();

        if ($id !== null && in_array(['id' => $id], $allIds)) {
            echo "You have just deleted category with id: " . $id . "<br>";
            $db = new SqlBuilder();
            $db->delete()->from('categories')->where('id', $id)->exec();
        } else {
            echo "ID input was empty or ID: " . $id . " dosent exist.";
        }
    }

    public function checkNameUnique($name)
    {
        $db = new SqlBuilder();
        $allNames = $db->select('name')->from('categories')->getAll();

        $allNames = array_merge_recursive(...array_values($allNames))["name"];

        if (in_array($name, $allNames) || $name === "" || $name === Null) {
            echo "Category with NAME: " . $name . " already exists or NAME input was empty.";
            return false;
        } else {
            echo "Category successfully added to database.";
            return true;
        }
    }

    public function getAllParentIds()
    {
        $db = new SqlBuilder();
        $allParentIds = $db->select('id')->from('categories')->getAll();

        $allParentIds = array_merge_recursive(...array_values($allParentIds))["id"];

        return $allParentIds;
    }
}
