<?php

namespace SausprShop\Categories\Controller;

use \SausprShop\Framework\Helper\FormBuilder;
use \SausprShop\Framework\Helper\Request;
use \SausprShop\Categories\Model\Category;

class Index
{
    private $request;

    public function __construct()
    {
        $this->request = new Request();
    }

    public function index()
    {
        $category = new Category();
        $category->load(2);
        print_r($category);
    }

    public function create()
    {
        $category = new Category();
        $allParentIds = $category->getAllParentIds();
        $allParentIds[] = "Root Category";

        var_dump($allParentIds);

        $formHelper = new FormBuilder('POST', 'store', 'form', 'category-form');
        $formHelper->input('text', 'name', 'text-field', 'category-name', 'Category Name');
        $formHelper->input('text', 'description', 'text-field', 'category-name', 'Category Description');
        $formHelper->select('parent_id', $allParentIds, 'Parent ID');
        $formHelper->button('ok', 'create');

        echo "Here you can create Category.";
        echo $formHelper->get();
    }

    public function store()
    {
        $name = $this->request->getPost('name');
        $description = $this->request->getPost('description');
        $parentId = $this->request->getPost('parent_id');

        $category = new Category();
        $category->setName($name);
        $category->setDescription($description);
        $category->setParentId($parentId);

        if ($category->checkNameUnique($name) === true) {
            $category->save();
        }
    }

    public function show()
    {
        $id = $this->request->getPost('id');

        $formHelper = new FormBuilder('POST', "show/$id", 'form', 'category-form');
        $formHelper->input('text', 'id', 'text-field', 'category-id', 'Category Id');
        $formHelper->button('show', 'Show One Category');
        $formHelper->button('showAll', 'Show All Categories');
        echo "Choose category ID to see category information. <br>";
        echo $formHelper->get();



        $category = new Category();

        var_dump($_POST);
        if ($id !== null || array_key_last($_POST) === "showAll") {
            $category->load($id);
        }
    }





    public function edit()
    {
        $idHrefFromShow = array_key_first($_POST);

        $edit = new FormBuilder('POST', "http://localhost:8001/index.php/categories/update", 'form', 'category-form');
        $edit->input('text', 'id', 'text-field', '', '', 'ID', "$idHrefFromShow")
            ->button('edit', 'ID Of category You Want To Edit');
        echo "You Can Choose Category To Edit.<br>";
        echo $edit->get();
    }

    public function update()
    {
        $id = $this->request->getPost('id');
        $category = new Category();
        $category->load($id);

        $id = $category->getId();
        $name = $category->getName();
        $description = $category->getDescription();
        $parentId = $category->getParentId();

        $update = new FormBuilder('POST', "http://localhost:8001/index.php/categories/update/$id", 'form', 'category-form');
        $update->input('hidden', 'id', '', '', '', '', "$id")
            ->input('text', 'name', 'text-field', '', '', 'name', "$name")
            ->input('text', 'description', 'text-field', '', '', 'description', "$description")
            ->input('text', 'parentId', 'text-field', '', '', 'parentId', "$parentId")
            ->button('update1', 'UPDATE');

        echo "Here You Can Edit Properties Of Chosen Category.<br>";
        echo $update->get();

        // var_dump($product);
        // var_dump($_POST);
        if (array_key_last($_POST) === "update1") {
            $category->save();
        }
    }

    public function delete()
    {
        $id = $this->request->getPost('id');
        $category = new Category();

        $idHrefFromShow = array_key_first($_POST);

        $delete = new FormBuilder('POST', "delete/" . $id, 'form', 'category-form');
        $delete->input('text', 'id', 'text-field', '', 'ID', "", " $idHrefFromShow")->button('delete', 'Delete');

        echo "HERE YOU CAN DELETE A CATEGORY.<br>";
        echo $delete->get();

        $category->delete($id);
    }
}
