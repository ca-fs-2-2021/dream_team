<?php

namespace SausprShop\Products\Model;

use \SausprShop\Framework\Helper\SqlBuilder;

class Product
{
    private $id;
    private $name;
    private $sku;
    private $price;
    private $qty;

    public function __construct($id = '')
    {
        if ($id !== '') {
            $this->load($id);
        }
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }
    public function getDescription()
    {
        return $this->Description;
    }
    public function getSpecialPrice()
    {
        return $this->SpecialPrice;
    }
    public function getCost()
    {
        return $this->Cost;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param mixed $qty
     */
    public function setQty($qty): void
    {
        $this->qty = $qty;
    }

    public function load($id)
    {
        $db = new SqlBuilder();
        $product = $db->select()->from('products')->where('id', $id)->getOne();

        $this->id = $product['id'];
        $this->name = $product['name'];
        $this->sku = $product['sku'];
        $this->price = $product['price'];
        $this->qty = $product['qty'];
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->create();
        }
    }

    private function update()
    {
        $db = new SqlBuilder();

        $product = [
            'name' => $this->name,
            'sku' => $this->sku,
            'price' => $this->price,
            'qty' => $this->qty,
        ];

        $db->update('products')->set($product)->where('id', $this->id)->exec();
    }

    private function create()
    {
        $product = [
            'name' => $this->name,
            'sku' => $this->sku,
            'price' => $this->price,
            'qty' => $this->qty,
        ];

        $db = new SqlBuilder();
        $db->insert('products')->values($product)->exec();
    }

    public function delete($id)
    {
        $db = new SqlBuilder();
        $product = $db->delete($id)->exec();
    }

    public function checkSkuUnix()
    {
        //
    }
}
