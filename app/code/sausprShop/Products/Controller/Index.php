<?php

namespace SausprShop\Products\Controller;

use \SausprShop\Framework\Helper\FormBuilder;
use \SausprShop\Framework\Helper\Request;
use \SausprShop\Products\Model\Product;


class Index
{
    private $request;

    public function __construct()
    {
        $this->request = new Request();
    }


    public function index()
    {
        $product = new Product();
        $product->load(2);
        print_r($product);
    }



    public function create()
    {
        $formHelper = new FormBuilder('POST', '/products/store', 'form', 'product-form');
        $formHelper->input('text', 'name', 'text-field', 'product-name', 'Product Name');
        $formHelper->input('text', 'sku', 'text-field', 'product-sku', 'Product Sku');
        $formHelper->input('text', 'price', 'text-field', 'product-price', 'Product Price');
        $formHelper->input('number', 'qty', 'text-field', 'product-qty', '', '', 'qty');
        $formHelper->button('ok', 'create');
        echo $formHelper->get();
    }
    // POST requestus
    public function store()
    {
        $name  = $this->request->getPost('name');
        $sku  = $this->request->getPost('sku');
        $price  = $this->request->getPost('price');
        $qty  = $this->request->getPost('qty');
        $product = new Product();
        $product->setName($name);
        $product->setSku($sku);
        $product->setPrice($price);
        $product->setQty($qty);
        $product->save();
    }
    public function show()
    {
        $id = $this->request->getPost('id');

        $formHelper = new FormBuilder('POST', "$id", 'form', 'product-form');
        $formHelper->input('text', 'id', 'text-field', 'product-id', 'Product Id');
        $formHelper->button('show', 'Show One Product');
        $formHelper->button('showAll', 'Show All Products');

        $product = new Product();

        var_dump($_POST);
        if ($id !== null || array_key_last($_POST) === "showAll") {
            $product->load($id);
        }
    }





    public function edit($id)
    {
        $product = new Product();
        $product->load($id);


        $name = $product->getName();
        $description = $product->getDescription();
        $sku = $product->getSku();
        $price = $product->getPrice();
        $specialPrice = $product->getSpecialPrice();
        $cost = $product->getCost();
        $qty = $product->getQty();

        $update = new FormBuilder('POST', "http://localhost:8001/index.php/products/update/$id", 'form', 'product-form');
        $update->input('hidden', 'id', '', '', '', 'name', "$id")
            ->input('text', 'name', 'text-field', '', '', '', "$name")
            ->input('text', 'description', 'text-field', '', '', 'description', "$description")
            ->input('text', 'sku', 'text-field', '', '', 'sku', "$sku")
            ->input('text', 'price', 'text-field', '', '', 'price', "$price")
            ->input('text', 'special_price', 'text-field', '', '', 'special_price', "$specialPrice")
            ->input('text', 'cost', 'text-field', '', '', 'cost', "$cost")
            ->input('text', 'qty', 'text-field', '', '', 'qty', "$qty")
            ->button('update1', 'UPDATE');
    }

    public function update()
    {
        $id = $this->request->getPost('id');
        $name = $this->request->getPost('name');
        $sku  = $this->request->getPost('sku');
        $price  = $this->request->getPost('price');
        $qty  = $this->request->getPost('qty');
        $product = new Product();
        $product->load($id);
        $product->setName($name);
        $product->setSku($sku);
        $product->setPrice($price);
        $product->setQty($qty);
        $product->save();
    }

    public function delete()
    {
        $id = $this->request->getPost('id');
        $product = new Product();

        $idHrefFromShow = array_key_first($_POST);

        $delete = new FormBuilder('POST', "delete/" . $id, 'form', 'product-form');
        $delete->input('text', 'id', 'text-field', '', 'ID', "", " $idHrefFromShow")->button('delete', 'Delete');

        echo "HERE YOU CAN DELETE A PRODUCT.<br>";
        echo $delete->get();

        $product->delete($id);
    }
}
