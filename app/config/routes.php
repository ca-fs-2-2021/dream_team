<?php

$routes = [
    'products' => 'SausprShop\Products\Controller\Index',
    'categories' => 'SausprShop\Categories\Controller\Index',
    'review' => 'SausprShop\Reviews\Controller\Index',
];
