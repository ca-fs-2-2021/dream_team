<?php
require_once './header.php';

$name = "";
$description = "";
$sku = "";
$price = "";
$special_price = "";
$cost = "";
$qty = "";
$update = false;
$id = "";

//delete function
include_once 'config/evn.php';
$queries = array();
parse_str($_SERVER['QUERY_STRING'], $queries);
if (isset($queries['action']) && $queries['id']) {
    $id = $queries['id'];
    if ($queries['action'] == 'delete') {
        $sql = 'DELETE FROM products WHERE id=' . $queries['id'];
        $conn->query($sql);
        echo 'Deleted successfully';
        die();
    } else if ($queries['action'] == 'edit' || $queries['action'] == 'addcat') {
        $result = $conn->query("SELECT * FROM products WHERE id=" . $queries['id']);

        if ($result->num_rows == 0) {
            echo "id does not exist";
            die();
        }
        $update = true;
        $row = $result->fetch_assoc();
        $name = $row['Name'];
        $description = $row['Description'];
        $sku = $row['Sku'];
        $price = $row['Price'];
        $special_price = $row['Special_price'];
        $cost = $row['Cost'];
        $qty = $row['Qty'];
    }
    if ($queries['action'] == 'addcat' && $_SERVER["REQUEST_METHOD"] == "POST") {

        $name = $_POST['name'];
        $description = $_POST['description'];

        $sql = "INSERT INTO products (Name, Description, parent_id) VALUES ('$name','$description',$id)";
        if (!mysqli_query($conn, $sql)) {
            echo "Error: something wrong" . $sql;
            echo $conn->error;
        }

        mysqli_close($conn);
    }
}
?>

<div class="container flex">

</div>
<div class="col-md-4">
    <div class="page-header">
        <h2>Add product</h2>
    </div>
    <form method="post" action="form.php<?php if ($update) echo '?edit=' . $id ?>">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="form-group">
            <label>Product Name</label>
            <input type="text" name="name" placeholder="Enter product name" class="form-control" value="<?php echo $name; ?>">
        </div>
        <div class="form-group">
            <label>Description</label>
            <input type="text" name="description" placeholder="Enter product description" class="form-control" value="<?php echo $description; ?>">
        </div>
        <div class="form-group">
            <label>Sku</label>
            <input type="text" name="sku" placeholder="Enter product sku" class="form-control" value="<?php echo $sku; ?>">
        </div>
        <div class="form-group">
            <label>Price</label>
            <input type="number" name="price" placeholder="Enter product price" class="form-control" value="<?php echo $price; ?>">
        </div>
        <div class="form-group">
            <label>Special Price</label>
            <input type="number" name="special_price" placeholder="Enter product special price" class="form-control" value="<?php echo $special_price; ?>">
        </div>
        <div class="form-group">
            <label>Cost</label>
            <input type="number" name="cost" placeholder="Enter product cost" class="form-control" value="<?php echo $cost; ?>">
        </div>
        <div class="form-group">
            <label>Qty</label>
            <input type="number" name="qty" placeholder="Enter product qty" class="form-control" value="<?php echo $qty; ?>">
        </div>
        <button type="submit" class='btn btn-primary'><?php echo ($update) ? 'Update' : 'Submit' ?></button>
    </form>
</div>
<!--product input area end-->
<?php if ($update) { ?>
    <table class='table table-striped '>
        <thead>
            <th>Name</th>
            <th>Description</th>
        </thead>
        <tbody>
            <?php
            include_once 'config/connections.php';
            $sql = "SELECT * FROM category WHERE parent_id=$id";
            $resultMysqlObj = $conn->query($sql);
            if ($resultMysqlObj->num_rows > 0) {
                // gavom bent viena eilute informacijos
                $result = $resultMysqlObj->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $row) {
                    echo "<tr>
                        <td>" . $row['Name'] . "</td>
                        <td>" . $row['Description'] . "</td>
                 <td>
                 <a href='/product.php?id=" . $row['id'] . " type='button' class='btn btn-danger'>Delete</a>
                 </td>
                        </tr>
                        ";
                }
            } else {
                // negavom nei vienos eilutes
                echo '<div class="alert alert-danger">0 eiluciu atitiko uzklausa</div>';
            }
            ?>


            </tr>
        </tbody>
    </table>

    <!-- categories input start -->
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <div class="page-header">
                        <h2>Categories</h2>
                    </div>
                    <form method="POST" action="<?php echo "product.php?id=$id" ?>&action=addcat">

                        <div class="form-group">
                            <label>Product Name</label>
                            <input type="text" name="name" placeholder="Enter product name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" name="description" placeholder="Enter product description" class="form-control">
                        </div>

                        <input type="hidden" name="parent_id" value="<?php echo $id; ?>">

                        <button type="submit" class='btn btn-primary'>Add</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>




<?php
require_once './footer.php';
?>