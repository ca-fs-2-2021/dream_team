<?php

require_once '../app/config/env.php';
require_once '../vendor/autoload.php';

$request = new \SausprShop\Framework\Helper\Request;
$routes = new \SausprShop\Framework\Helper\Routing;

$url = $request->getRoute();

$routes->getControllerClass($url);
