<?php
require_once './header.php';


?>
<!--product input area-->


<div class="content">
    <table class='table table-striped '>
        <thead>
            <th>Name</th>
            <th>Description</th>
            <th>SKU</th>
            <th>Price</th>
            <th>Special price</th>
            <th>Cost</th>
            <th>Qty</th>
            <th>Actions</th>
        </thead>
        <tbody>
            <?php
            include_once 'config/connections.php';
            $sql = 'SELECT * FROM products';
            $resultMysqlObj = $conn->query($sql);
            if ($resultMysqlObj->num_rows > 0) {
                // gavom bent viena eilute informacijos
                $result = $resultMysqlObj->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $row) {
                    echo "<tr>
                        <td>" . $row['Name'] . "</td>
                        <td>" . $row['Description'] . "</td>
                        <td>" . $row['Sku'] . "</td>
                        <td>" . $row['Price'] . "</td>
                        <td>" . $row['Special_price'] . "</td>
                        <td>" . $row['Cost'] . "</td>
                        <td>" . $row['Qty'] . "</td>
                        <td>
                 <a href='/product.php?id=" . $row['id'] . "&action=edit' type='button' class='btn btn-info'>Edit</a>
                 <a href='/product.php?id=" . $row['id'] . "&action=delete' type='button' class='btn btn-danger'>Delete</a>
                 </td>
                        </tr>
                        ";
                }
            } else {
                // negavom nei vienos eilutes
                echo '<div class="alert alert-danger">0 eiluciu atitiko uzklausa</div>';
            }
            ?>


            </tr>
        </tbody>
    </table>
</div>
<?php
require_once './footer.php';
